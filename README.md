# Debian container builder

This dockerfile builds a minimal Debian container with debootstrap.

Build command example

```
docker build -t local/debian:bookworm-slim --build-arg="DEB_SUITE=bookworm" --progress=plain .
```
