FROM debian:bookworm-slim AS builder

ARG DEB_MIRROR=http://deb.debian.org/debian
ARG DEB_SUITE=bookworm

RUN apt-get -qq update && \
    apt-get -q install --assume-yes debootstrap fakeroot && \
    fakeroot debootstrap --variant=minbase ${DEB_SUITE} /src ${DEB_MIRROR}

FROM scratch

LABEL maintainer="PCLL Team"

COPY --from=builder /src .
RUN echo "APT::Get::Assume-Yes \"true\";" | tee /etc/apt/apt.conf.d/10-assume_yes && \
    apt-get remove --purge -y $(apt-mark showauto) && rm -rf /var/lib/apt/lists/* && \
    apt-get remove --allow-remove-essential e2fsprogs e2fslibs whiptail && \
    apt-get -y autoremove && apt-get clean && \
    rm -rf /var/lib/apt/lists /var/cache/apt /usr/share/man \
    /usr/share/locale /var/log /usr/share/info
CMD ["bash"]
